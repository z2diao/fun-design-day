using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class CSharpTutorial {
    static public void Main() {
        // instantiate an array to hold our shapes
        Shape[] a = new Shape[3]; 
        // instantiate some shapes into the array
        a[0] = new Rectangle(0, 0, 3, 4);
        a[1] = new Triangle(7, 7, 3, 4, 90);
        a[2] = new Circle(9, 9, 2);
        // draw them
        for (int i=0; i < a.Length; i++) {
            a[i].Draw();
        }
    }
}
abstract class Shape {
    public int x; public int y;
    public Shape() {
        this.x = 0; this.y = 0;
    }
    public Shape(int x, int y) {
        this.x = x; this.y = y;
    }
    abstract public void Draw();
}
class Rectangle : Shape { 
    int width; int height;
    public Rectangle(int x, int y, int w, int h) : base(x, y) {
        this.width = w; this.height = h;
    }
    public override void Draw() {
        Console.WriteLine("Rectangle:  " + width + " by " + height + " at " + x + "," + y);
    }
}
class Triangle : Shape { 
    int side1; int side2; int angle;
    public Triangle(int x, int y, int s1, int s2, int a) : base(x, y) {
        this.side1 = s1; this.side2 = s2; this.angle = a;
    }
    public override void Draw() {
        Console.WriteLine("Triangle:   side1=" + side1 + " side2=" + side2 + " angle=" + angle + " at " + x + "," + y);
    }
}
class Circle : Shape { 
    int radius;
    public Circle(int x, int y, int r) : base(x, y) {
        this.radius = r;
    }
    public override void Draw() {
        Console.WriteLine("Circle:     radius=" + radius + " at " + x + "," + y);
    }
}
